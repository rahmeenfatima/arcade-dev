import React from 'react'

import GameScreen from '../Screens/GameScreen';

const GameContainer = () =>  {
  return (
    <div>
        <GameScreen/>
    </div>
  );
}

export default GameContainer;