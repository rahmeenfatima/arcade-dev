import React from 'react'
import './style.css'
import arcade from '../../img/arcade.svg'
import icon from '../../img/icon.svg'
import Buttons from '../Buttons/index'
import Inputs from '../Inputs/index'


const LoginScreen = () => {
    return (
        <div className='flexing1'>
            <div className='background1'>
                <img src={icon} className='icon1' />
                <img src={arcade} className='arcade' />
                <Buttons placeholder="email"/>
                <Buttons placeholder="password"/>
                <Buttons placeholder="Login"/>
                <div className='or1'>or</div>
                <Inputs/>
                <div className='account1'>
                    <div className='need1'>Need an account? </div>
                    <a href='#' className='sign-up1'><b> SIGN-UP </b></a>
                </div>

            </div>

        </div>

    );
}
export default LoginScreen; 