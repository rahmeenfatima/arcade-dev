import React from 'react'
import './style.css'
import arcade from '../../img/arcade.svg'
import icon from '../../img/icon.svg'
import Buttons from '../Buttons/index'
import Inputs from '../Inputs/index'
import Accept from '../Accept/index'
const SignUp = () => {
    return (
        <div className='flexing'>
            <div className='background'>
                <img src={icon} className='icon' />
                <img src={arcade} className='arcade' />
                <Buttons placeholder="email" />
                <Buttons placeholder="password" />
                <Buttons placeholder="First Name" />
                <Buttons placeholder="Last Name" />
                <Buttons placeholder="Birthday" />
                <Buttons placeholder="Register" />
                <div className='or'> or</div>
                <Inputs />
                <div className="container">
                    <input type="checkbox" className='' />
                    <Accept accept="accept" signup="T&C"/>
                </div>
                <div className="container2">
                    <input type="checkbox" />
                    <label> subscribe to newsletter</label>
                </div>
                <Accept accept="Already a user?" signup="LOGIN" />
            </div>
        </div>

    );
}
export default SignUp;