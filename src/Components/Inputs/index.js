import React from 'react'
import './style.css'
import facebook from '../../img/face-book.svg'
import google from '../../img/google.svg'


const Inputs = () => {
    return (
        <div>
            <div className='facebook-icon'>
                <div className='face-book'>
                    <img src={facebook} className='face' />
                </div>
                <div className='book'>
                    {/* <div> </div> */}
                    <div> Facebook</div>
                </div>
            </div>

            <div className='google-icon'>
                <img src={google} className='google' />
                <div className='google-word'>Google</div>
            </div>
        </div>

    );
}
export default Inputs;