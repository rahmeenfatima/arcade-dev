import React from 'react'
import './style.css'

const Accept = ({accept, signup }) => {
    return (
        <div className='account'>
        <div className='need'>{accept}</div>
        <a href='#' className='sign-up'><b> {signup} </b></a>
    </div>
    );
}
export default Accept;