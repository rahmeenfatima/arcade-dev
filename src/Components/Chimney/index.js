import React from 'react'
import './style.css'
import chimney from '../../img/chimney-logo-color.svg'
import chimney2 from '../../img/chimney-logo.svg'
import user from '../../img/chimney-user.svg'
import password from '../../img/chimney-password.svg'
// import background from '../../img/background2.png'
const Chimney = () => {
    return (
        <div className='chimney-main'>
            <div className='margin'>
                <div className='chimney-logo'>
                    <img src={chimney} className='logo' />
                    <div className='chimney'><b>chimney</b></div>
                </div>
                <div className='welcome'><b>Welcome to Chimney</b></div>
                <div className='lorem'>Lorem ipsum doloramet, consecte sit amet, consectetur adimodo<br />
                    consequat. ipsum dolor et, consectetur adimodo consequat.</div>
                <div className='email-icon'>
                    <img src={user} className='user' /><br />
                    <div>
                        <label className='address'>Email address</label><br />
                        <input className='email' value="example@gmail.cl" />
                    </div>
                </div>
                <div className='email-icon'>
                    <img src={password} className='user' /><br />
                    <div>
                        <label className='address'>password</label><br />
                        <input className='email' placeholder='Enter your password' />
                    </div>
                </div>
                <button className='create'> Create Account </button>
                <div className='link'>
                    <a href='#'>I already have an account </a>
                </div>
                {/* <img src={background} className='background'/> */}

            </div>
            {/* //////////////     FOOTER      ///////////////////// */}
            <div className='main'>
                <div>
                    <div className='chimney-logo'>
                        <img src={chimney2} className='logo' />
                        <div className='chimney'><b>chimney</b></div>
                    </div>
                    <div className='branded'>This is a branded product<br />
                        description. This is a branded<br />
                        product description.
                    </div>
                </div>
                <div>
                    <table>
                        <tr>
                            <th>COMPANY</th>
                            <th>PRODUCT</th>
                            <th>RESOURCES</th>
                        </tr>
                        <tr>
                            <td> About OChain  </td>
                            <td>Create on NFT</td>
                            <td> Testimonials</td>
                        </tr>
                        <tr>
                            <td>Twitter</td>
                            <td>Explore NFTs</td>
                            <td> Support</td>
                        </tr>
                        <tr>
                            <td>Blog</td>
                            <td>OChain</td>
                            <td>Terms</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td> Privacy</td>
                        </tr>
                    </table>


                </div>


            </div>

        </div>

    );
}
export default Chimney;