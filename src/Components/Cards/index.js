import React from 'react'
import './style.css'
import eye from '../../img/eye.svg'
import game from '../../img/game.svg'
const ScrollBar = () => {
    return (
        <div>
            <div className='card-main'>
                <div className='card'>
                    <div className='end'>
                        <div> </div>
                        <div className='views'>
                            <img src={eye} className='eye' />
                            <div className='twofive6'><b> 256</b></div>
                        </div>
                    </div>
                    <div className='main-game'>
                        <div></div>
                        <img src={game} className='game' />
                    </div>
                </div>
                <div className='machine'> Machine #1</div>
            </div>

        </div>

    );
}
export default ScrollBar; 