import React from 'react'
import './style.css'
import rightarrow from '../../img/right-arrows.svg'
import leftarrow from '../../img/left-arrows.svg'
import Cards from '../Cards/index'
const ScrollBar = ({title}) => {
    return (
        <div className='home'>
            <div className='brown'>
                <img src={rightarrow} className='right-arrow' />
                <div className='standard-line'>
                    <div className='line1'> </div>
                    <div className='standard'>{title} </div>
                    <div className='line1'> </div>
                </div>
                <img src={leftarrow} className='left-arrow' />
            </div>
            <div className='main-scroll'>
                <div className='scroll'>
                    <Cards />
                    <Cards />
                    <Cards />
                    <Cards />
                    <Cards />
                    <Cards />
                    <Cards />


                </div>

            </div>

        </div>


    );
}
export default ScrollBar; 