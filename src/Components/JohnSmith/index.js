import React from 'react'
// styles and asserts
import './style.css'
import arcade from '../../img/arcade.svg'
import eye from '../../img/eye.svg'
import john from '../../img/John.svg'
import jennifer from '../../img/Jennifer.svg'
import scoth from '../../img/Scoth.svg'
import mark from '../../img/Mark.svg'
import mari from '../../img/Mari.svg'
import dominik from '../../img/Dominik.svg'
import deni from '../../img/Deni.svg'
import monica from '../../img/Monica.svg'
import scoth2 from '../../img/Scoth2.svg'
import marian from '../../img/Marian.svg'
import john2 from '../../img/John2.svg'
import big from '../../img/Big.svg'
import game1 from '../../img/game1.svg'
import union from '../../img/Union.svg'
import drop from '../../img/Drop.svg'
import circle from '../../img/circle.svg'
import diamond from '../../img/diamond.svg'
import pokeball from '../../img/pokeball.svg'
import userf from '../../img/user-f.svg'
import sound from '../../img/sound.svg'
import diamondf from '../../img/diamond-f.svg'

const JohnSmith = () => {

  const Card = ({ name, image }) => {
    return (
      <div className='john-smith'>
        <img src={image} className='john' />
        <div className='smith'>{name}</div>
      </div>
    )
  }
  const PrizeName =() => {
    return (
      <div className='prize-name'>
      <div className='boll'>  <img src={pokeball} className='pokeball' /> </div>
      <div className='name'>Prize Name</div>
    </div>
    )
  }

  return (
    <div className='marin'>
      <div className='header2'>
        <img src={arcade} className='arcade3' />
        <div className='eight-credits'>
          <div className='eight'>8</div>
          <div className='credits'>Credits</div>
        </div>
        <div className='john-smith1'>
          <img src={john} className='john' />
          <div className='smith'>John Smith</div>
        </div>
      </div>

      <div className='responsive-header'>
        <img src={arcade} className='arcade3r' />
        <img src={john} className='john-r' />
        <div className='eight-credits-r'>
          <div className='eight'>8</div>
          <div className='credits'>Credits</div>
        </div>
      </div>

      {/* ///////////CRDS////////// */}
      <div className='flexes2'>
        {/* ////////////      CARD1     //////////////////////*/}
        <div className='card-one'>
          <div className='header3'>
            <div className='john-smith1'>
              <img src={john} className='john' />
              <div className='smith'>John Smith</div>
            </div>
            <div className='viewer-count'>
              <img src={eye} className='eye2' />
              <div className='two-five'>256</div>
              <div className='viewer'>viewer count</div>
            </div>
          </div>
          <div className='responsive-header3'>
            <div className='john-smith1-r'>
              <img src={john} className='john' />
              <div className='smith'>John Smith</div>
            </div>
            <div className='viewer-count-r'>
              <img src={eye} className='eye2' />
              <div className='two-five'>256</div>
            </div>
          </div>

          <div className='viewers'>
            <Card name={'Jennifer'} image={ jennifer} />
            <Card name={'Scoth'} image={scoth}/>
            <Card name={'Mark'} image={mark} />
            <Card  name={'Mari'} image={mari}/>
            <Card  name={'Dominik'} image={dominik}/>
            <Card  name={'Deni'} image={deni}/>
            <Card  name={'Monica'} image={monica}/>
            <Card  name={'John Smith'} image={john}/>
            <Card  name={'Scoth'} image={scoth2}/>
            <Card  name={'Marian'} image={marian}/>
            <Card  name={'John'} image={john2}/>
            <Card  name={'John'} image={john2}/>
            <Card  name={'John'} image={john2}/>
            <Card  name={'John'} image={john2}/>
            <Card  name={'John'} image={john2}/>
            <Card  name={'John'} image={john2}/>
            <Card  name={'John'} image={john2}/>
        
          </div>
          <div className='footer'>
            <img src={userf} className='userf' />
            <img src={sound} className='users' />
            <img src={diamondf} className='userf' />
          </div>
        </div>
        {/* ////////////////////// CARD2  ///////////////////////////// */}
        <div className='card2'>
          <div className='responsive-header-two'>
            <img src={arcade} className='arcade3r' />
            <img src={john} className='john-r' />
            <div className='eight-credits-r'>
              <div className='eight'>8</div>
              <div className='credits'>Credits</div>
            </div>
          </div>
          <div className='card-five'>
            <div className='views2'>
              <img src={eye} className='eye3' />
              <div className='two-five'>256</div>
            </div>
            <div className='main-game'>
              <img src={game1} className='part-img' />
              <img src={big} className='big-img' />
            </div>

            <img src={union} className='union' />
            <div className='vew'>view</div>
            <img src={drop} className='drop' />
          </div>
          <div className='footer-2'>
            <img src={userf} className='userf' />
            <img src={sound} className='users' />
            <img src={diamondf} className='userf' />
          </div>
        </div>
        {/*/////////////////////      CARD3        /////////////////////  */}
        <div className='card6-main'>
          <div className='responsive-header3'>
            <div className='john-smith1-r'>
              <img src={john} className='john' />
              <div className='smith'>John Smith</div>
            </div>
            <div className='viewer-count-r'>
              <img src={eye} className='eye2' />
              <div className='two-five'>256</div>
            </div>
          </div>
          <div className='prize'><b>Prize List</b></div>
          <div className='padding'>
            <div className='card6'>
              <img src={circle} className='circle' />
              <img src={diamond} className='diamond' />
            </div>
            <div className='jackpot'><b>JACKPOT</b></div>

            <div className='prize-prize'>
              
              <PrizeName/>
              <PrizeName/>
              <PrizeName/>
              <PrizeName/>
              <PrizeName/>
              <PrizeName/>
              <PrizeName/>
            </div>
          </div>
          <div className='footer-3'>
            <img src={userf} className='userf' />
            <img src={sound} className='users' />
            <img src={diamondf} className='userf' />
          </div>

        </div>
      </div>
    </div>
  );
}
export default JohnSmith; 