import React from 'react'
import './style.css'
import arcade from '../../img/arcade.svg'
import ScrollBar from '../ScrollBar/index'

const HomeScreen = () => {
    return (
        <div>
            <div className='header'>
                <img src={arcade} className='arcade2' />
                <div className='buttons'>
                    <div className='login'><a href='#'>  Log In </a></div>
                    <div className='signin'> Sign In</div>
                </div>
            </div>
            <div className='bg-black'> 
            <ScrollBar title="STANDARD"/>
            <ScrollBar title="EXPENSIVE"/>
            <ScrollBar title="HIGH ROLLER" />
            <ScrollBar title="STANDARD"/>
            </div>

         </div>


    );
}
export default HomeScreen; 